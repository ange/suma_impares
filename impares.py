# Repositorio plantilla: "Suma de números impares"

#Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el enunciado, que incluye la fecha de entrega.


x= int(input("Dame un número entero no negativo: "))
y=int(input("Dame otro: "))

suma=0

if x>y:
    x,y=y,x

for numero in range(x,1+y):
        if numero%2!=0:
            suma=suma+numero

print(suma)